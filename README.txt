Installation Instructions:

1. Copy the uc_payu_india module directory into your modules directory.

2. Enable uc_payu_india module at: Administer > Modules (admin/modules)

Author: Tom Blauwendraat <antiflu@gmail.com>
Portions based on Drupal 6 'uc_securepayu' module
